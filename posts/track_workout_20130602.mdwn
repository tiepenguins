[[!meta title="Track Workout (6/2/2013)"]]

Our track workout this week is thanks to Neil!


Set up
======

Mark out a track 50 meters long with markers every ten meters.

Workout
=======

60 [burpees](http://www.youtube.com/watch?v=21dvQyNiTjM)
60 [mountain climbers](http://www.youtube.com/watch?v=tOWjEAW3huU)
with [suicides](http://www.youtube.com/watch?v=MPL487ToJt8) in between

start on one side of the track, complete 10 burpees here then use the
markers on the track to do suicide runs to get to the other side where
you will complete 10 mountain climbers (4 count) then run the suicides
again to get back. Repeat this circuit without stopping until rep
counts for both burpees and mountain climbers are satisfied. You
should be finishing with a suicide run *not* the last exercise.
Suicides are all out sprints.


Things to Focus On
===================

- agility during suicides: turn arounds should be as close to instant
  as you can possibly get. When you get to each line work on getting a
  good plant with your cleat and lower your bodyweight into the turn
  then explode out in the opposite direction.

- Pace: forget about it completely. This workout is supposed to be all
  out as hard as you can go with no rests, you will get tired, you
  will slow down. The point is to recognize what happens to your 100%
  mark when you subject yourself to a constant intense physical load.
  Don't get discouraged if you slow down, know that it will happen and
  just keep pushing as hard as you can through to the end.

- Screw you Neil, I have to take a rest: Thats fine, Just try to keep
  the rests short, no longer than 30 seconds or about the time it
  takes to grab a couple sips of water (so keep your water bottle
  close). The shorter the rest, the better. That said, really pay
  attention to the difference between "This is really hard" and "I
  really can't do any more"

- Upper time limit: This workout should take anywhere from 25 to 45
  minutes subjectively but If you are pushing 45 minutes and you are
  nowhere near done with the workout, call it quits. Once you pass
  that point the benefits gained from the workout start tapering off
  quickly, its better to call it and fight that fight another day. If
  you stay consistent you will get faster, guaranteed.

How to do things
================
1. [Video of burpees](http://www.youtube.com/watch?v=21dvQyNiTjM)
2. [Video of mountain climbers](http://www.youtube.com/watch?v=tOWjEAW3huU)
3. [Video of suicides](http://www.youtube.com/watch?v=MPL487ToJt8)


[[!tag track]]
